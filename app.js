const express = require(`express`);
const mongoose = require(`mongoose`);

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const mongo_opts = { useCreateIndex: true, useNewUrlParser: true };
mongoose.connect(`${process.env.MONGO_URL}`, mongo_opts)
    .then(() => console.log(`Mongoose connected.`));

/**
 * Allow CORS
 */
app.use((req, res, next) => {
    res.header(`Access-Control-Allow-Origin`, `*`);
    res.header(`Access-Control-Allow-Headers`,
        `Origin, X-Requested-Width, Content-Type, Accept, Authorization`);
    if (req.method == `OPTIONS`) {
        res.header(`Access-Control-Allow-Methods`,
                   `PUT, GET, POST, PATCH, DELETE`);
        return res.status(200).json({});
    }
    next();
});

if (!process.env.PRODUCTION)
    app.use(require(`morgan`)(`dev`));

require(`./router`)(app);

/**
 * Add the static frontend to '/'
 */
app.use(require(`connect-history-api-fallback`)());
app.use(express.static(__dirname + `/front/dist`));

app.listen(process.env.PORT || 3000, function() {
    console.log(`Server running on port ${this.address().port}`);
});
