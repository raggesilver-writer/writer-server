const router = require(`express`).Router();
const User = require(`../models/user`);
const guard = require(`../guard`);

router.post(`/validate`, guard.validate, (req, res) => {
    return res.status(200).send(`Ok`);
});

router.post(`/register`, async (req, res) => {
	try {
		// Check if the email is already in use
		if (await User.findOne({email: req.body.email}))
			return res.status(409).end(`Email in use.`);

		let user = new User({
			email: req.body.email,
			password: await User.hashPassword(req.body.password)
		});

		await user.save();
		next();
	} catch(e) {
		console.error(e);
		res.status(500).end(`Internal error.`);
	}
});

router.post(`/login`, async (req, res) => {
	try {
		let user = await User.findOne({email: req.body.email});
		if (!user) return res.status(401).end(`Invalid credentials.`);
		let pass = await user.comparePassword(req.body.password);
		if (!pass) return res.status(401).end(`Invalid credentials.`);
		return res.end(user.getToken());
	} catch(e) {
		console.error(e);
		return res.status(500).end(`Internal error.`);
	}
});

module.exports = router;
