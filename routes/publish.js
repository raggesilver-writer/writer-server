const router = require(`express`).Router();
const guard = require(`../guard`);
const Article = require(`../models/article`);
const validator = require(`express-joi-validation`)({
    passError: true
});
const Joi = require(`joi`);

const pub_schema = Joi.object({
    title: Joi.string().required(),
    content: Joi.string().required().min(1),
    fileid: Joi.string().required().min(7)
});

router.post('/', guard.validate,
                 validator.body(pub_schema),
                 async (req, res) => {
	try {
		let article = await Article.findOne({ fileid: req.body.fileid });

		if (article) {
            if (req.user._id.toString() != article.author.toString())
                return res.status(401).end("This article is not yours!");

            // Update article
            if (req.body.title)
                article.title = req.body.title;
            article.content = req.body.content;
            article.modified = new Date();

            await article.save();

            return res.status(200).end(article._id.toString());
        } else {

			article = new Article({
				author: req.user._id,
                content: req.body.content,
                title: req.body.title,
				published: new Date(),
				fileid: req.body.fileid
            });

            article.modified = article.published;

            await article.save();

			return res.status(201).end(article._id.toString());
		}
	} catch(e) {
		console.error(e);
		return res.status(500).end("Internal error.");
	}
});

router.use((err, req, res, next) => {
    console.log(err);
    if (err.error.isJoi) {
        return res.status(400).end(err.error.toString());
    } else {
        next(err);
    }
});

module.exports = router;
