const router = require(`express`).Router();
const Article = require(`../models/article`);
const User = require(`../models/user`);

router.get('/', async (req, res) => {
	let articles = await Article.find().populate(`author`, `_id`);

	articles = articles.sort((a, b) => {
		return (new Date(b.modified) - new Date(a.modified));
	});

	return res.json(articles);
});

router.get(`/view/:id`, async (req, res) => {
	try {
        let article = await Article.findById(req.params.id)
            .select('-filied').populate(`author`, `name`);
        if (article)
            return res.json(article);
        return res.status(404).end(`Article not found`);
    } catch(e) {
        return res.status(400).end(`Invalid article id`);
    }
});

module.exports = router;
