module.exports = (app) => {

    const routes = [
        { url: '/api/auth', path: 'auth' },
        { url: '/api/publish', path: 'publish' },
        { url: '/api/', path: 'home' },
    ];

    routes.forEach(route => {
        console.log('Setup', route);
        app.use(`${route.url}`, require(`./routes/${route.path}`));
    });

}
