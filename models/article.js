const mongoose = require(`mongoose`);

let article = new mongoose.Schema({
	author: { type: mongoose.Types.ObjectId, ref: 'user' },
	content: { type: String },
	published: { type: Date },
	modified: { type: Date },
    fileid: { type: String },
    title: { type: String }
});

module.exports = mongoose.model("article", article);
