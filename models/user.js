const mongoose = require(`mongoose`);
const bcrypt = require(`bcrypt`);
const jwt = require(`jsonwebtoken`);

let user = new mongoose.Schema({
    name: { type: String },
	email: { type: String, unique: true, required: true },
	password: { type: String }
});

user.statics.hashPassword = (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, 10, (err, hash) => {
            if (err) return reject(err);
            return resolve(hash);
        });
    });
};

user.methods.comparePassword = function (password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.password, (err, match) => {
            if (err) return reject(err);
            return resolve(match);
        })
    });
};

user.methods.getToken = function () {
    return jwt.sign({
        _id: this._id
    }, process.env.JWT_KEY);
};

module.exports = mongoose.model("user", user);
