import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

import Home from './components/Home';
import View from './components/View';
import NotFound from './components/NotFound';

import BootstrapVue from 'bootstrap-vue';

const defaultTitle = 'Writer Articles';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(BootstrapVue);

/**
 * Set the routes
 */
const router = new VueRouter({
  mode: 'history', // No # at the beginning of the URL
  routes: [
    { path: `/`, component: Home },
    { path: `/view/:id`, component: View },
    { path: `/404`, component: NotFound },
    { path: `*`, redirect: `/404` }
  ]
});

// Allow route to specify a page title
router.beforeEach((to, from, next) => {
  document.title = to.meta.title || defaultTitle;
  next();
});

Vue.prototype.$user = null;
Vue.prototype.$http = require('axios');

new Vue({
  router,
  render: h => h(App),
  beforeCreate: function () {
    this.$user = localStorage.getItem('user');
    this.$http.defaults.baseURL = process.env.API_BASE || 'https://ragge.site';
  }
}).$mount('#app');
