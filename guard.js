const jwt = require(`jsonwebtoken`);
const assert = require(`assert`);
const User = require(`./models/user`);

module.exports = {
  validate: async (req, res, next) => {
    try {
      let bearer = req.headers[`authorization`].split(` `)[1];
      let tk = jwt.verify(bearer, process.env.JWT_KEY);
      assert(tk);
      req.user = await User.findById(tk._id).select(`-password`);
      next();
    } catch(e) {
      res.status(401).json({error: `invalid-token`});
    }
  }
};
